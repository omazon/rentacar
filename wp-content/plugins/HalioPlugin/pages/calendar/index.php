<div class="halio-settings-page">
	<h1 class="center"><?php
    _e('Calendar', 'halio');
  ?></h1>

  <div id="calendar">
    <span class="halio-calendar-loading-text"><?php
      _e('Loading Calendar...', 'halio');
    ?></span>
  </div>
</div>
