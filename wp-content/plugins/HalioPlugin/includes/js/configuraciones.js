
function configuraciones(){
    if(jQuery('.price').text()>0){
        var fecha = jQuery('#HalioPickupTime').val();
        var duracion_element = jQuery('#HalioDuration').val();
        var duracion = calcular_duracion(duracion_element);
        var precio_final = init_configuraciones(duracion);
        mostrar_resultado(horas_pico(fecha),precio_final,duracion);
    }
}
function init_configuraciones(duracion){
    //capturando datos
    var vehiculo = jQuery('#HalioVehicleType').val(); //2 = Sedan, 3 = SUV A, 4 = SUV B
    if(vehiculo==1){
        var precio_final = calcular_precio(duracion,7);
    }
    if(vehiculo==2){
        var precio_final = calcular_precio(duracion,11);
    }
    if(vehiculo==3){
        var precio_final = calcular_precio(duracion,9);
    }
    return precio_final;
}
function calcular_duracion(texto){
    var minuto = texto / 60;
    return Math.round(minuto);
}
function calcular_precio(duracion,precio_inicial) {
    var a = Math.floor(duracion / 30); //calcular el intervalo y redondear el menor valor
    var b = 30 * a; //calcular minutos correspondiente al intérvalo
    var c = duracion - b; //tiempo sobrado después del intérvalo, ayuda a encontrar periodo de gracia
    if(c <= 2){
        var gracia = 5;
    }else{
        var gracia = 10;
    }//determino el periodo de gracia 5 o 10 min
    if(c > gracia){
        a = a+1; //dependiendo del periodo de gracia, cobro la siguiente media hora
    }
    var precio_final = precio_inicial * a;
    return precio_final;
}

function mostrar_resultado(hora_pico,precio_final,duracion){
    if(hora_pico){
        duracion = duracion * 2;
        if(duracion >= 35){
            precio_final = precio_final * 2;
        }
    }
    jQuery('.duration_new').html(cambio_hora(duracion));
    jQuery('.new_price').html(precio_final);
    jQuery('.new_price').removeClass('hidden');
};
function horas_pico (fecha){
    var format, format_fecha, dia_semana, inicio_matutino, final_matutino, inicio_vespertino, final_vespertino;
    format = 'HH:mm';
    format_fecha = moment(fecha, 'DD/MM/YYYY HH:mm').format(format);
    dia_semana = moment(fecha, 'DD/MM/YYYY HH:mm').format('dddd');
    format_fecha = moment(format_fecha, format);
    inicio_matutino = moment('06:29', format);
    final_matutino = moment('08:29', format);
    inicio_vespertino = moment('16:59', format);
    final_vespertino = moment('19:29', format);
    return((format_fecha.isBetween(inicio_matutino,final_matutino) || format_fecha.isBetween(inicio_vespertino,final_vespertino)) && (dia_semana !== 'Saturday' && dia_semana!== 'Sunday'));
}
function cambio_hora(duracion){
    var hora, minuto;
    hora = moment.duration(duracion,'minutes').asHours();
    minuto = moment.duration(duracion,'minutes').minutes();
    if(hora < 1 ){
        return minuto + " min";
    }else{
        return Math.trunc(hora) + "h " + minuto + " min";
    }

}
