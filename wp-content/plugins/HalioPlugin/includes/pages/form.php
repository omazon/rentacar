<?php

global $flash;
global $wpdb;
global $woocommerce;

$vehicles = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "halio_vehicles` WHERE `is_active` = 1");
$verticle_class = '';
$half_width_column = 'col-sm-4';
$quarter_width_column = 'col-sm-3';
$map_column_width = 'col-sm-8';

if ( isset($options['vertical']) ) {
  $options['vertical'] = $options['vertical'];
}

if ($options['verticle'] == 'true') {
  $half_width_column = 'col-md-12';
  $quarter_width_column = 'col-md-6';
  $verticle_class = 'verticle';
  $map_column_width = 'col-md-12';
}

if ( class_exists('WooCommerce') ) {
  ?><div class="row halio-form-container <?= $verticle_class; ?>"><?php
    if ( isset($flash) ) {
      ?><div class="alert alert-<?= $flash['type']; ?>" role="alert"><?=
        $flash['message'];
      ?></div><?php
    }

    ?>
<div class="col-md-4 halio-left-container height-full">
    <div class="car-image">
        <img src="<?php echo halio_plugin_url();?>/includes/css/carro.png" alt="">
        <div class="halio-separator"></div>
    </div>
      <form class="halio-form" method="post" action="">
        <input type="hidden" id="HalioDistance" name="halio_distance_in_meters">
        <input type="hidden" id="HalioPrettyDistance" name="halio_distance">
        <input type="hidden" id="HalioDuration" name="halio_duration_in_seconds">
        <input type="hidden" id="HalioPrettyDuration" name="halio_duration">
        <input type="hidden" id="HalioStartingLat" name="halio_starting_coords_lat">
        <input type="hidden" id="HalioStartingLong" name="halio_starting_coords_long">
        <input type="hidden" id="HalioDestinationLat" name="halio_destination_coords_lat">
        <input type="hidden" id="HalioDestinationLong" name="halio_destination_coords_long">
        <input type="hidden" id="HalioMapStartingCountry" value="<?= halio_get_settings_row('map_starting_country')->value; ?>">
        <input type="hidden" id="HalioUnitSystem" value="<?= halio_get_settings_row('units')->value; ?>">
        <input type="hidden" id="HalioPrice" name="halio_price">
        <input type="hidden" id="HalioMinuteBuffer" name="halio_minute_buffer" value="<?= halio_get_settings_row('form_booking_buffer_time_minutes')->value; ?>"><?php
        if ( halio_get_settings_row('enforce_autocomplete_country_restriction')->value ) {
          ?><input type="hidden" id="HalioAutocompleteRestriction" value="<?= halio_get_settings_row('autocomplete_country')->value; ?>"><?php
        }

        ?><div class="halio-input-overlay-group">
          <div class="halio-overlay">
            <i class="fa fa-map-marker white-text font-20"></i>
          </div><?php
          if ( halio_get_settings_row('use_fixed_addresses_for_origin')->value ) {
            $starting_addresses = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "halio_fixed_addresses` WHERE `is_active` = 1 AND `origin_or_destination` = 'origin'");

            ?><select id="HalioStartingAddressSelect" name="halio_starting_address">
              <option><?= halio_get_settings_row('form_starting_address_select_text')->value; ?></option><?php
              foreach ($starting_addresses as $starting_address) {
                ?><option value="<?= $starting_address->address; ?>"><?= $starting_address->pretty_address; ?></option><?php
              }
            ?></select><?php
          } else {
            ?>
              <input class="white-text border bg-blue mb-10 br-10" type="text" id="HalioStartingAddress" aria-describedby="starting-address-status" name="halio_starting_address" placeholder="<?= halio_get_settings_row('form_starting_address_label')->value; ?>" autofocus><?php
          }
        ?></div>

        <div class="halio-input-overlay-group">
          <div class="halio-overlay">
              <i class="fa fa-car white-text font-20"></i>
          </div><?php
          if ( halio_get_settings_row('use_fixed_addresses_for_destination')->value ) {
            $destination_addresses = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "halio_fixed_addresses` WHERE `is_active` = 1 AND `origin_or_destination` = 'destination'");

            ?><select id="HalioDestinationAddressSelect" name="halio_destination_address">
              <option><?= halio_get_settings_row('form_destination_address_select_text')->value; ?></option><?php
              foreach ($destination_addresses as $destination_address) {
                ?><option value="<?= $destination_address->address; ?>"><?= $destination_address->pretty_address; ?></option><?php
              }
            ?></select><?php
          } else {
            ?><input class="white-text border bg-blue br-10 mb-10" type="text" id="HalioDestinationAddress" aria-describedby="destination-address-status" name="halio_destination_address" placeholder="<?= halio_get_settings_row('form_destination_address_label')->value; ?>"><?php
          }
        ?></div><?php

        if (
          !halio_get_settings_row('use_fixed_addresses_for_origin')->value ||
          !halio_get_settings_row('use_fixed_addresses_for_destination')->value
        ) {
          ?><div class="halio-marker-helper-container" style="display: none;">
            <span class="halio-marker-helper"><?php
              if (
                !halio_get_settings_row('use_fixed_addresses_for_origin')->value &&
                !halio_get_settings_row('use_fixed_addresses_for_destination')->value
              ) {
                _e('Drag the markers to change location', 'halio');
              } else if ( !halio_get_settings_row('use_fixed_addresses_for_destination')->value ) {
                _e('Drag the destination marker to change location', 'halio');
              } else if ( !halio_get_settings_row('use_fixed_addresses_for_origin')->value ) {
                _e('Drag the origin marker to change location', 'halio');
              }
            ?></span>
          </div>
            <?php
        }
    if ( halio_get_settings_row('form_can_edit_vehicle_type')->value ) {
    ?>
    <select class="form-control mb-10 height-50 bg-blue white-text border br-10 text-center font-18" id="HalioVehicleType" name="halio_vehicle_id">
        <option disabled selected><?= halio_get_settings_row('form_vehicle_type_label')->value; ?>...</option><?php
//		foreach ($vehicles as $vehicle) {
//			?><!--<option value="--><?//= $vehicle->id; ?><!--" data-max-occupants="--><?//= $vehicle->passenger_space; ?><!--">--><?//=
//			$vehicle->name;
//			?><!--</option>--><?php
//		}
		?>
        <optgroup class="bg-white blue-text" label="Sedán">
            <option class="bg-white blue-text" value="1" data-max-occupants="5">Yaris</option>
            <option value="1" data-max-occupants="5">Swift Dzire</option>
        </optgroup>
        <optgroup class="bg-white blue-text" label="SUV A">
            <option class="bg-white blue-text" value="3" data-max-occupants="5">Prado</option>
            <option class="bg-white blue-text" value="3" data-max-occupants="5">Fortuner</option>
            <option class="bg-white blue-text" value="3" data-max-occupants="5">Sorento</option>
        </optgroup>
        <optgroup class="bg-white blue-text" label="SUV B">
            <option class="bg-white blue-text" value="2" data-max-occupants="5">Rav4</option>
            <option class="bg-white blue-text" value="2" data-max-occupants="5">Sportage</option>
        </optgroup>
    </select>
    <div class="halio-input-container return-pick-up-time" style="display: none;">
            <label for="HalioReturnPickupTime"><?= halio_get_settings_row('form_return_pick_up_time_label')->value; ?></label>

            <div class="input-right" id="HalioReturnPickupTimeContainer">
                <input type="text" class="form-control" id="HalioReturnPickupTime" name="halio_return_pick_up_time" placeholder="<?= halio_get_settings_row('form_return_pick_up_time_label')->value; ?>">
            </div>
        </div>
    <div class="input-right" id="HalioPickupTimeContainer">
        <input type="text" class="form-control bg-blue white-text border br-10 font-18 height-50 mb-10" id="HalioPickupTime" name="halio_pick_up_time" placeholder="<?= halio_get_settings_row('form_pick_up_time_label')->value; ?>">
    </div>
    <a href="#" style="white-space: pre-line;" class="btn bg-orange white-text font-bold width-full br-10 font-20 mayus estimate-cost" data-disable-with="<i class='fa fa-refresh fa-spin'></i>" data-original-text="<?= halio_get_settings_row('form_estimate_cost_label')->value; ?>" data-estimating-text="<?= halio_get_settings_row('form_estimating_cost_label')->value; ?>"><?=
        halio_get_settings_row('form_estimate_cost_label')->value;
        ?></a>
	<?php
}
?>
        <div class="alert alert-warning halio-form-feedback" role="alert" style="display: none;"></div>

        <div class="estimate-container br-10 bg-blue no-padding border" style="display: none;">
          <div class="halio-price-container center">
            <span class="currency white-text"><?= get_woocommerce_currency_symbol(); ?></span>
            <span class="price hidden">0.00</span>
            <span class="new_price white-text" style="font-weight: bold;font-size: 2.5em;">0.00</span>
          </div>

          <ul class="journey-stats center bg-white blue-text br-time mb-20"><?php
            if (halio_get_settings_row('form_show_duration_in_estimate')->value) {
              ?><li class="stat">
                <span class="fa fa-clock-o"></span>
                <span class="duration hidden"></span>
                <span class="duration_new"></span>
              </li><?php
            }

            if (halio_get_settings_row('form_show_distance_in_estimate')->value) {
              ?><li class="stat">
                <span class="fa fa-road"></span>
                <span class="distance"></span>
              </li><?php
            }
          ?></ul>
        </div>

        <div class="center halio-booking-button-container" style="display: none;">
          <input type="submit" value="<?= halio_get_settings_row('form_book_button_text')->value; ?>" style="display: none;" class="btn btn-success booking-button re-booking-no">
          <span class="re-booking-info blue-text">
            <i class="fa fa-phone" aria-hidden="true"></i> +505 2255 6757<br/>
            <i class="fa fa-whatsapp" aria-hidden="true"></i> +505 8940 0040
          </span>
        </div>
      </form>
    </div><?php

    if ($options['verticle'] == 'true') {
      ?>

        <div class="col-sm-7 halio-right-container halio-verticle-map-container">
        <div id="map"></div>
      </div><?php
    } else {
      ?><div class="col-sm-1"></div>
    <div class="col-sm-7 halio-right-container height-full">
      <div id="map"></div>
        <span class="grey-text">* Modelo de los vehículos pueden cambiar acorde a disponibilidad</span>
    </div><?php
    }
  ?></div><?php
} else {
  ?>You need to install WooCommerce. Without it Halio will not work.<?php
}
