<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'rentacar_taxi');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '5+x,*Ro)efzx.K@Z7{W?D&ydMe~;)Su3fI)O/aMuTp&:l~@|yw^DblME_(?QkX*v');
define('SECURE_AUTH_KEY', 'J}KqsYF/fA[mF~Nq(ew<woWl;CUXr6*A-4!?F1&D6hx~A5vB&$!=Prj#}C#g9p}6');
define('LOGGED_IN_KEY', '!TErklK|`:OlktkcN#d%TzEMNa.7zR-pN2A!v^xl4Ke*A3)SYhR_aB>O85)umoVY');
define('NONCE_KEY', 'ytVt40`d%6A5FcBim5De&y^6qIK(uURWGqZAr>v<o4DS>3#B>M3W=usz3n5Py&-e');
define('AUTH_SALT', '+?64.egw5Fj0zN]2<Ck$rm.32/ssCs2ADg.KSv@5;v|)%fEzc2aU:>/NQsw 9e}+');
define('SECURE_AUTH_SALT', '_|EBqQ,uI+290(GL$u&.V(/LAbf:eP^MJFTIJZuzq[`gun47cAppt|,7V`V6ga]_');
define('LOGGED_IN_SALT', 'N1^6b]3_D=6;tSDGf9XpNc+k~q8RE)m@V3k@Sk_dR=/(.H$_L_OT4iC8vBxB9:]u');
define('NONCE_SALT', 'a3m7Hftf@~W4B5*g@(|Z8IZ3;?Vs^i+pluDTNT;V0Y4hoJZ8W7U.wWv59KORc3M:');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

